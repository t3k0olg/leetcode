import java.util.HashMap;
import java.util.Map;

class Solution {
    public int minSteps(String s, String t) {
        Map<Character,Integer> hm = new HashMap<>();
        int counter = 0;
        for( int i = 0; i < s.length(); i++ ){
            if( hm.containsKey( s.charAt(i) ) )
                hm.put( s.charAt(i), hm.get( s.charAt(i) ) + 1 );
            else 
                hm.put( s.charAt(i), 1 );
        }
        
        for( int i = 0; i < t.length();i++ ){
            if( hm.containsKey( t.charAt(i) ) && hm.get( t.charAt(i) ) > 0 )
                hm.replace( t.charAt(i), hm.get( t.charAt(i)  ) - 1 );
            else{
                counter++;
            } 
                
        }
        return counter;
    }
}