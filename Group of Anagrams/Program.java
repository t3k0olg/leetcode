import java.util.*;

class Program {
  public static List<List<String>> groupAnagrams(List<String> words) {
		Map<String,List<String>> hs = new HashMap<String,List<String>>();
    for( String word: words  ){
			
			char tempWordArray [] = word.toCharArray();
			Arrays.sort( tempWordArray );
			String sortedWord = new String( tempWordArray );
			
			if( hs.containsKey(sortedWord) )
				hs.get(sortedWord).add( word );
			else
				hs.put( sortedWord, new ArrayList<String>( Arrays.asList(word) ) );
			
		}
    return new ArrayList<>(hs.values());
  }
}
