class Solution {
    int [] esPosicionGanadoraParaElJugadorActual = new int[1001];
    public boolean divisorGame(int N) {
        
        setUp();
        
        boolean esTurnoAlice = true;
        return calcula(N,esTurnoAlice) == 1;
    }
    
    public int calcula(int N, boolean esTurnoAlice){
        if( esPosicionGanadoraParaElJugadorActual[N] != -1 ){
            if( esTurnoAlice ) 
                return esPosicionGanadoraParaElJugadorActual[N] == 1 ? 1 : 0;
        }
        return esPosicionGanadoraParaElJugadorActual[N] = (calcula(N - 1, !esTurnoAlice) + 1) % 2;
        
    }
    
    public void setUp(){
        for( int i = 0; i <= 1000; i++ ) 
            esPosicionGanadoraParaElJugadorActual[i] = -1;
        /* asignamos casos base*/
        esPosicionGanadoraParaElJugadorActual[0] = 0;
        esPosicionGanadoraParaElJugadorActual[1] = 0;
        esPosicionGanadoraParaElJugadorActual[2] = 1;
    }
}