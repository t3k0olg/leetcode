class Solution {
    public static void main(String[] args) {
        customSortString("cba","cbbaaabddd");
    }
    public static String customSortString(String S, String T) {
        Map<Character,Integer> hs = new HashMap<>();
        StringBuilder ans = new StringBuilder();
        
        for( int i = 0; i < T.length(); i++){
            char c = T.charAt( i );
            if( !hs.containsKey( c ) )
                hs.put( c, 1 );
            else
                hs.put(c, hs.get(c) + 1 );
        }
        
        for( int i = 0;  i < S.length();i++ ){
            char c = S.charAt( i );
            if( hs.containsKey(c) ){
                int reps = hs.get(c);
                for( int j = 0; j < reps;j++){
                    ans.append(c);
                }
                hs.remove(c);
            }
        }
        
        if( !hs.isEmpty()){
            for(Map.Entry m:hs.entrySet()){    
                int reps = (Integer) m.getValue();
                for( int i = 0; i < reps ;i++ )
                    ans.append( m.getKey() );
            }  
        }
        return ans.toString();
    }
}

